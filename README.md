# Kladionice-MNE

## intro
There is a plethora of bookmakers & casinos in Montenegro. The goal of this small project is to raise awareness of this abnormal number and perhaps save some young people from getting addicted to gambling.  

The locations are taken from their official sites but also from Google Maps since some didn't publish their locations on their sites. It is possible that some locations aren't pinpoint precise, or even missing. So you can help by contributing :)

The interactive map can be found here: https://gjovani.gitlab.io/kladionice

----
## contributing

You can contribute by editing the file `kladionice.csv`  

(note: the column `verified` just means the locations that have been verified in person)

the csv is converted to geojson using https://github.com/mapbox/csv2geojson with the following command:  
`csv2geojson kladionice.csv > kladionice.geojson`

note: add `var kla =` in the beginning of the .geojson file
